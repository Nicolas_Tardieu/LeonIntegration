import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  public cabinetsNb = '3';
  public consultantsNb = '86';
  public profitsNb = '36';

}
